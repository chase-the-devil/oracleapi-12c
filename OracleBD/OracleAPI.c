//******************************************************
//***				Made by Mr-Cas					****
//******* Oracle API (Tested on Oracle 12c)	************
//******************************************************

#include "OracleAPI.h"
#include <stdlib.h>

unsigned __int8 type_ora_to_native(ora_data_type ora_type)
	{
		switch(ora_type & 0xffffffff00000000)
			{
				case ORA_TYPE_VARCHAR2:
				case ORA_TYPE_CHAR:
				case ORA_TYPE_LONG:
					return NATIVE_TYPE_STRING;

				case ORA_TYPE_NUMBER:
				case ORA_TYPE_RAW:
				case ORA_TYPE_LONG_RAW:
					return NATIVE_TYPE_BYTE_ARRAY;

				case ORA_TYPE_SIGNED_INTEGER:
					{
						switch(ora_type & 0x00000000ffffffff)
							{
								case sizeof(__int8):
									return NATIVE_TYPE_INT8;

								case sizeof(__int16):
									return NATIVE_TYPE_INT16;

								case sizeof(__int32):
									return NATIVE_TYPE_INT32;

								case sizeof(__int64):
									return NATIVE_TYPE_INT64;
							}

						return -1;
					}

				case ORA_TYPE_FLOAT:
					{
						switch(ora_type & 0x00000000ffffffff)
							{
								case sizeof(float):
									return NATIVE_TYPE_FLOAT;

								case sizeof(double):
									return NATIVE_TYPE_DOUBLE;
							}

						return -1;
					}

				case ORA_TYPE_STRING:
				case ORA_TYPE_CHARZ:
					return NATIVE_TYPE_NULL_TERMINATED_STRING;

				case ORA_TYPE_VARNUM:
					return NATIVE_TYPE_VARNUM;

				case ORA_TYPE_VARCHAR:
					return NATIVE_TYPE_VARCHAR;

				case ORA_TYPE_DATE:
					return NATIVE_TYPE_DATE;

				case ORA_TYPE_VARRAW:
					return NATIVE_TYPE_VARRAW;

				case ORA_TYPE_BINARY_FLOAT:
					return NATIVE_TYPE_FLOAT;

				case ORA_TYPE_BINARY_DOUBLE:
					return NATIVE_TYPE_DOUBLE;

				case ORA_TYPE_UNSIGNED_INTEGER:
					return NATIVE_TYPE_UNSIGNED_INT;

				case ORA_TYPE_LONG_VARCHAR:
					return NATIVE_TYPE_LONG_VARCHAR;

				case ORA_TYPE_LONG_VARRAW:
					return NATIVE_TYPE_LONG_VARRAW;
			}

		return -1;
	}

short type_ora_to_c(ora_data_type ora_type)
	{
		switch(ora_type & 0xffffffff00000000)
			{
				case ORA_TYPE_VARCHAR2:
					return C_TYPE_VARCHAR2;

				case ORA_TYPE_NUMBER:
					return C_TYPE_NUMBER;

				case ORA_TYPE_SIGNED_INTEGER:
					return C_TYPE_SIGNED_INTEGER;

				case ORA_TYPE_FLOAT:
					return C_TYPE_FLOAT;

				case ORA_TYPE_STRING:
					return C_TYPE_NULL_TERMINATED_STRING;

				case ORA_TYPE_VARNUM:
					return C_TYPE_VARNUM;

				case ORA_TYPE_LONG:
					return C_TYPE_LONG;

				case ORA_TYPE_VARCHAR:
					return C_TYPE_VARCHAR;

				case ORA_TYPE_DATE:
					return C_TYPE_DATE;

				case ORA_TYPE_VARRAW:
					return C_TYPE_VARRAW;

				case ORA_TYPE_BINARY_FLOAT:
					return C_TYPE_BINARY_FLOAT;

				case ORA_TYPE_BINARY_DOUBLE:
					return C_TYPE_BINARY_DOUBLE;

				case ORA_TYPE_RAW:
					return C_TYPE_RAW;

				case ORA_TYPE_LONG_RAW:
					return C_TYPE_LONG_RAW;

				case ORA_TYPE_UNSIGNED_INTEGER:
					return C_TYPE_UNSIGNED_INTEGER;

				case ORA_TYPE_LONG_VARCHAR:
					return C_TYPE_LONG_VARCHAR;

				case ORA_TYPE_LONG_VARRAW:
					return C_TYPE_LONG_VARRAW;

				case ORA_TYPE_CHAR:
					return C_TYPE_CHAR;

				case ORA_TYPE_CHARZ:
					return C_TYPE_CHARZ;
			}

		return -1;
	}

//***			cud getters

short* ora_get_cud_connect()
	{
		const short cud[] = 
			{
				CUD_HEADER,
				CUD_HEADER_OFFSET, CUD_CONNECT
			};

		unsigned int len = sizeof(short) * CUD_CONNECT_LENGTH;

		short* ret_cud = malloc(len);

		memcpy(ret_cud, cud, len);

		return ret_cud;
	}

short* ora_get_cud_connect_identified()
	{
		const short cud[] = 
			{
				CUD_HEADER,
				CUD_HEADER_OFFSET, CUD_CONNECT_IDENTIFIED
			};

		unsigned int len = sizeof(short) * CUD_CONNECT_IDENTIFIED_LENGTH;

		short* ret_cud = malloc(len);

		memcpy(ret_cud, cud, len);

		return ret_cud;
	}

short* ora_get_cud_commit_work()
	{
		const short cud[] = 
			{
				CUD_HEADER,
				CUD_HEADER_OFFSET, CUD_COMMIT_WORK
			};

		unsigned int len = sizeof(short) * CUD_COMMIT_WORK_LENGTH;

		short* ret_cud = malloc(len);

		memcpy(ret_cud, cud, len);

		return ret_cud;
	}

short* ora_get_cud_commit_work_release()
	{
		const short cud[] = 
			{
				CUD_HEADER,
				CUD_HEADER_OFFSET, CUD_COMMIT_WORK_RELEASE
			};

		unsigned int len = sizeof(short) * CUD_COMMIT_WORK_RELEASE_LENGTH;

		short* ret_cud = malloc(len);

		memcpy(ret_cud, cud, len);

		return ret_cud;
	}

short* ora_get_cud_alter_session(short stmt_len)
	{
		const short cud[] = 
			{
				CUD_HEADER,
				CUD_HEADER_OFFSET, CUD_ALTER_SESSION
			};

		unsigned int len = sizeof(short) * CUD_ALTER_SESSION_LENGTH;

		short* ret_cud = malloc(len);

		memcpy(ret_cud, cud, len);

		ret_cud[ALTER_SESSION_STMT_LEN_INDEX]		= stmt_len;

		return ret_cud;
	}

short* ora_get_cud_create_user(short stmt_len)
	{
		const short cud[] = 
			{
				CUD_HEADER,
				CUD_HEADER_OFFSET, CUD_CREATE_USER
			};

		unsigned int len = sizeof(short) * CUD_CREATE_USER_LENGTH;

		short* ret_cud = malloc(len);

		memcpy(ret_cud, cud, len);

		ret_cud[CREATE_USER_STMT_LEN_INDEX]	= stmt_len;

		return ret_cud;
	}

short* ora_get_cud_drop_user(short stmt_len)
	{
		const short cud[] = 
			{
				CUD_HEADER,
				CUD_HEADER_OFFSET, CUD_DROP_USER
			};

		unsigned int len = sizeof(short) * CUD_DROP_USER_LENGTH;

		short* ret_cud = malloc(len);

		memcpy(ret_cud, cud, len);

		ret_cud[DROP_USER_STMT_LEN_INDEX]		= stmt_len;

		return ret_cud;
	}

short* ora_get_cud_grant_user(short stmt_len)
	{
		const short cud[] = 
			{
				CUD_HEADER,
				CUD_HEADER_OFFSET, CUD_GRANT_USER
			};

		unsigned int len = sizeof(short) * CUD_GRANT_USER_LENGTH;

		short* ret_cud = malloc(len);

		memcpy(ret_cud, cud, len);

		ret_cud[GRANT_USER_STMT_LEN_INDEX]		= stmt_len;

		return ret_cud;
	}

short* ora_get_cud_open_cursor(short id_cursor, short stmt_len)
	{
		const short cud[] = 
			{
				CUD_HEADER,
				CUD_HEADER_OFFSET, CUD_OPEN_CURSOR
			};

		unsigned int len = sizeof(short) * CUD_OPEN_CURSOR_LENGTH;

		short* ret_cud = malloc(len);

		memcpy(ret_cud, cud, len);

		ret_cud[OPEN_CURSOR_ID_INDEX]			= id_cursor;
		ret_cud[OPEN_CURSOR_STMT_LEN_INDEX]		= stmt_len;

		return ret_cud;
	}

short* ora_get_cud_close_cursor(short id_cursor)
	{
		const short cud[] = 
			{
				CUD_HEADER,
				CUD_HEADER_OFFSET, CUD_CLOSE_CURSOR
			};

		unsigned int len = sizeof(short) * CUD_CLOSE_CURSOR_LENGTH;

		short* ret_cud = malloc(len);

		memcpy(ret_cud, cud, len);

		ret_cud[CLOSE_CURSOR_ID_INDEX]			= id_cursor;

		return ret_cud;
	}

short* ora_get_cud_fetch_into(short id_cursor, short param_count, ora_data_type* ora_param_types)
	{
		const short cud[] = 
			{
				CUD_HEADER,
				CUD_HEADER_OFFSET, CUD_FETCH_INTO
			};

		unsigned int	cud_len = (CUD_FETCH_INTO_LENGTH + (param_count * 4));
		short*			ret_cud = malloc(sizeof(short) * cud_len);

		memcpy(ret_cud, cud, sizeof(short) * 20);

		ret_cud[FETCH_INTO_CURSOR_ID_INDEX]		= id_cursor;
		ret_cud[FETCH_INTO_PARAM_COUNT_INDEX]	= param_count;

		unsigned int param_index = 0;

		for(unsigned int index = FETCH_INTO_PARAMS_INDEX; index < cud_len; index += 4)
			{
				ret_cud[index		] = 2;
				ret_cud[index + 1	] = type_ora_to_c(ora_param_types[param_index]);
				ret_cud[index + 2	] = 0;
				ret_cud[index + 3	] = 0;
				++param_index;
			}

		return ret_cud;
	}

short* ora_get_cud_create_table(short stmt_len)
	{
		const short cud[] = 
			{
				CUD_HEADER,
				CUD_HEADER_OFFSET, CUD_CREATE_TABLE
			};

		unsigned int len = sizeof(short) * CUD_CREATE_TABLE_LENGTH;

		short* ret_cud = malloc(len);

		memcpy(ret_cud, cud, len);

		ret_cud[CREATE_TABLE_STMT_LEN_INDEX]	= stmt_len;

		return ret_cud;
	}

short* ora_get_cud_drop_table(short stmt_len)
	{
		const short cud[] = 
			{
				CUD_HEADER,
				CUD_HEADER_OFFSET, CUD_DROP_TABLE
			};

		unsigned int len = sizeof(short) * CUD_DROP_TABLE_LENGTH;

		short* ret_cud = malloc(len);

		memcpy(ret_cud, cud, len);

		ret_cud[DROP_TABLE_STMT_LEN_INDEX]		= stmt_len;

		return ret_cud;
	}

short* ora_get_cud_alter_table(short stmt_len)
	{
		const short cud[] = 
			{
				CUD_HEADER,
				CUD_HEADER_OFFSET, CUD_ALTER_TABLE
			};

		unsigned int len = sizeof(short) * CUD_ALTER_TABLE_LENGTH;

		short* ret_cud = malloc(len);

		memcpy(ret_cud, cud, len);

		ret_cud[ALTER_TABLE_STMT_LEN_INDEX]		= stmt_len;

		return ret_cud;
	}

short* ora_get_cud_update_table(short stmt_len)
	{
		const short cud[] = 
			{
				CUD_HEADER,
				CUD_HEADER_OFFSET, CUD_UPDATE_TABLE
			};

		unsigned int len = sizeof(short) * CUD_UPDATE_TABLE_LENGTH;

		short* ret_cud = malloc(len);

		memcpy(ret_cud, cud, len);

		ret_cud[UPDATE_TABLE_STMT_LEN_INDEX]	= stmt_len;

		return ret_cud;
	}

short* ora_get_cud_insert_into(short stmt_len)
	{
		const short cud[] = 
			{
				CUD_HEADER,
				CUD_HEADER_OFFSET, CUD_INSERT_INTO
			};

		unsigned int len = sizeof(short) * CUD_INSERT_INTO_LENGTH;

		short* ret_cud = malloc(len);

		memcpy(ret_cud, cud, len);

		ret_cud[INSERT_INTO_STMT_LEN_INDEX]		= stmt_len;

		return ret_cud;
	}

short* ora_get_cud_delete_from(short stmt_len)
	{
		const short cud[] = 
			{
				CUD_HEADER,
				CUD_HEADER_OFFSET, CUD_DELETE_FROM
			};

		unsigned int len = sizeof(short) * CUD_DELETE_FROM_LENGTH;

		short* ret_cud = malloc(len);

		memcpy(ret_cud, cud, len);

		ret_cud[DELETE_FROM_STMT_LEN_INDEX]		= stmt_len;

		return ret_cud;
	}

short* ora_get_cud_enable_threads()
	{
		const short cud[] = 
			{
				CUD_HEADER,
				CUD_HEADER_OFFSET, CUD_ENABLE_THREADS
			};

		unsigned int len = sizeof(short) * CUD_ENABLE_THREADS_LENGTH;

		short* ret_cud = malloc(len);

		memcpy(ret_cud, cud, len);

		return ret_cud;
	}

short* ora_get_cud_create_context()
	{
		const short cud[] = 
			{
				CUD_HEADER,
				CUD_HEADER_OFFSET, CUD_CREATE_CONTEXT
			};

		unsigned int len = sizeof(short) * CUD_CREATE_CONTEXT_LENGTH;

		short* ret_cud = malloc(len);

		memcpy(ret_cud, cud, len);

		return ret_cud;
	}

short* ora_get_cud_free_context()
	{
		const short cud[] = 
			{
				CUD_HEADER,
				CUD_HEADER_OFFSET, CUD_FREE_CONTEXT
			};

		unsigned int len = sizeof(short) * CUD_FREE_CONTEXT_LENGTH;

		short* ret_cud = malloc(len);

		memcpy(ret_cud, cud, len);

		return ret_cud;
	}


//***		sql query interface

int ora_enable_threads()
	{
		unsigned int	exdSz	= sqlexdBaseSize;
		exd_struct		exd		= malloc(exdSz);

		memset(exd, 0, exdSz);

		_sqlvsn(exd)			= 13;
		_arrsiz(exd)			= 0;
		_sqladtp(exd)			= &sqladt;
		_sqltdsp(exd)			= &sqltds;
		_stmt(exd)				= "";
		_iters(exd)				= 1;
		_offset(exd)			= CUD_HEADER_OFFSET;
		_cud(exd)				= ora_get_cud_enable_threads();
		_sqlest(exd)			= (unsigned char*)(&_sqlca);
		_sqlety(exd)			= 4352;
		_occurs(exd)			= 0;

		sqlcxt((void **)0, &sqlctx, exd, &sqlfpn);

		free(_cud(exd));
		free(exd);

		return SQLCA_ERROR_CODE;
	}

sql_context ora_create_context()
	{
		unsigned int	exdSz	= sqlexdBaseSize + sqlexdParamSize;
		exd_struct		exd		= malloc(exdSz);

		memset(exd, 0, exdSz);

		sql_context context = ORA_NULL;

		_sqlvsn(exd)	= 13;
		_arrsiz(exd)	= 1;
		_sqladtp(exd)	= &sqladt;
		_sqltdsp(exd)	= &sqltds;
		_stmt(exd)		= "";
		_iters(exd)		= 1;
		_offset(exd)	= CUD_HEADER_OFFSET;
		_cud(exd)		= ora_get_cud_create_context();
		_sqlest(exd)	= (unsigned char*)(&_sqlca);
		_sqlety(exd)	= 4352;
		_occurs(exd)	= 0;

		_sqhstv(exd, _arrsiz(exd))[0] = &context;
		_sqhstl(exd, _arrsiz(exd))[0] = sizeof(void*);
		_sqhsts(exd, _arrsiz(exd))[0] = 0;
		_sqindv(exd, _arrsiz(exd))[0] = 0;
		_sqinds(exd, _arrsiz(exd))[0] = 0;
		_sqharm(exd, _arrsiz(exd))[0] = 0;
		_sqadto(exd, _arrsiz(exd))[0] = 0;
		_sqtdso(exd, _arrsiz(exd))[0] = 0;

		_sqphsv(exd)	= _sqhstv(exd, _arrsiz(exd));
		_sqphsl(exd)	= _sqhstl(exd, _arrsiz(exd));
		_sqphss(exd)	= _sqhsts(exd, _arrsiz(exd));
		_sqpind(exd)	= _sqindv(exd, _arrsiz(exd));
		_sqpins(exd)	= _sqinds(exd, _arrsiz(exd));
		_sqparm(exd)	= _sqharm(exd, _arrsiz(exd));
		_sqparc(exd)	= _sqharc(exd, _arrsiz(exd));
		_sqpadto(exd)	= _sqadto(exd, _arrsiz(exd));
		_sqptdso(exd)	= _sqtdso(exd, _arrsiz(exd));

		sqlcxt((void **)0, &sqlctx, exd, &sqlfpn);

		free(_cud(exd));
		free(exd);

		return context;
	}

int ora_free_context(sql_context context)
	{
		unsigned int	exdSz	= sqlexdBaseSize + sqlexdParamSize;
		exd_struct		exd		= malloc(exdSz);

		memset(exd, 0, exdSz);

		_sqlvsn(exd)	= 13;
		_arrsiz(exd)	= 1;
		_sqladtp(exd)	= &sqladt;
		_sqltdsp(exd)	= &sqltds;
		_stmt(exd)		= "";
		_iters(exd)		= 1;
		_offset(exd)	= CUD_HEADER_OFFSET;
		_cud(exd)		= ora_get_cud_free_context();
		_sqlest(exd)	= (unsigned char*)(&_sqlca);
		_sqlety(exd)	= 4352;
		_occurs(exd)	= 0;

		_sqhstv(exd, _arrsiz(exd))[0] = &context;
		_sqhstl(exd, _arrsiz(exd))[0] = sizeof(void*);
		_sqhsts(exd, _arrsiz(exd))[0] = 0;
		_sqindv(exd, _arrsiz(exd))[0] = 0;
		_sqinds(exd, _arrsiz(exd))[0] = 0;
		_sqharm(exd, _arrsiz(exd))[0] = 0;
		_sqadto(exd, _arrsiz(exd))[0] = 0;
		_sqtdso(exd, _arrsiz(exd))[0] = 0;

		_sqphsv(exd)	= _sqhstv(exd, _arrsiz(exd));
		_sqphsl(exd)	= _sqhstl(exd, _arrsiz(exd));
		_sqphss(exd)	= _sqhsts(exd, _arrsiz(exd));
		_sqpind(exd)	= _sqindv(exd, _arrsiz(exd));
		_sqpins(exd)	= _sqinds(exd, _arrsiz(exd));
		_sqparm(exd)	= _sqharm(exd, _arrsiz(exd));
		_sqparc(exd)	= _sqharc(exd, _arrsiz(exd));
		_sqpadto(exd)	= _sqadto(exd, _arrsiz(exd));
		_sqptdso(exd)	= _sqtdso(exd, _arrsiz(exd));

		sqlcxt((void **)0, &sqlctx, exd, &sqlfpn);

		free(_cud(exd));
		free(exd);

		return SQLCA_ERROR_CODE;
	}

int ora_exd_connect(char* stmt, short mode, sql_context* context)
	{
		unsigned int	exdSz			= sqlexdBaseSize + sqlexdParamSize;
		exd_struct		exd				= malloc(exdSz);

		memset(exd, 0, exdSz);

		_sqladtp(exd)					= &sqladt;
		_sqltdsp(exd)					= &sqltds;
		
		_sqlvsn(exd)					= 13;
		_arrsiz(exd)					= 1;
		_sqlest(exd)					= (unsigned char*)(&_sqlca);
		_sqlety(exd)					= 4352;

		_cud(exd)						= ora_get_cud_connect();
		_iters(exd)						= 10;
		_offset(exd)					= CUD_HEADER_OFFSET;
		_occurs(exd)					= 0;
		_sqhstv(exd, _arrsiz(exd))[0]	= stmt;
		_sqhstl(exd, _arrsiz(exd))[0]	= 0;
		_sqhsts(exd, _arrsiz(exd))[0]	= 0;
		_sqindv(exd, _arrsiz(exd))[0]	= 0;
		_sqinds(exd, _arrsiz(exd))[0]	= 0;
		_sqharm(exd, _arrsiz(exd))[0]	= mode;
		_sqadto(exd, _arrsiz(exd))[0]	= 0;
		_sqtdso(exd, _arrsiz(exd))[0]	= 0;
		_sqphsv(exd)					= _sqhstv(exd, _arrsiz(exd));
		_sqphsl(exd)					= _sqhstl(exd, _arrsiz(exd));
		_sqphss(exd)					= _sqhsts(exd, _arrsiz(exd));
		_sqpind(exd)					= _sqindv(exd, _arrsiz(exd));
		_sqpins(exd)					= _sqinds(exd, _arrsiz(exd));
		_sqparm(exd)					= _sqharm(exd, _arrsiz(exd));
		_sqparc(exd)					= _sqharc(exd, _arrsiz(exd));
		_sqpadto(exd)					= _sqadto(exd, _arrsiz(exd));
		_sqptdso(exd)					= _sqtdso(exd, _arrsiz(exd));
		_sqlcmax(exd)					= 100;
		_sqlcmin(exd)					= 2;
		_sqlcincr(exd)					= 1;
		_sqlctimeout(exd)				= 0;
		_sqlcnowait(exd)				= 0;

		sqlcxt(context, &sqlctx, exd, &sqlfpn);

		free(_cud(exd));
		free(exd);

		return SQLCA_ERROR_CODE;
	}

int ora_exd_connect_identified(char* stmtName, char* stmtPass, short mode, sql_context* context)
	{
		unsigned int	exdSz			= sqlexdBaseSize + (sqlexdParamSize << 1);
		exd_struct		exd				= malloc(exdSz);

		memset(exd, 0, exdSz);

		_sqladtp(exd)					= &sqladt;
		_sqltdsp(exd)					= &sqltds;

		_sqlvsn(exd)					= 13;
		_arrsiz(exd)					= 2;
		_sqlest(exd)					= (unsigned char*)(&_sqlca);
		_sqlety(exd)					= 4352;

		_cud(exd)						= ora_get_cud_connect_identified();
		_iters(exd)						= 10;
		_offset(exd)					= CUD_HEADER_OFFSET;
		_occurs(exd)					= 0;
										  
		_sqhstv(exd, _arrsiz(exd))[0]	= stmtName;
		_sqhstl(exd, _arrsiz(exd))[0]	= 0;
		_sqhsts(exd, _arrsiz(exd))[0]	= 0;
		_sqindv(exd, _arrsiz(exd))[0]	= 0;
		_sqinds(exd, _arrsiz(exd))[0]	= 0;
		_sqharm(exd, _arrsiz(exd))[0]	= mode;
		_sqadto(exd, _arrsiz(exd))[0]	= 0;
		_sqtdso(exd, _arrsiz(exd))[0]	= 0;
		_sqhstv(exd, _arrsiz(exd))[1]	= stmtPass;
		_sqhstl(exd, _arrsiz(exd))[1]	= 0;
		_sqhsts(exd, _arrsiz(exd))[1]	= 0;
		_sqindv(exd, _arrsiz(exd))[1]	= 0;
		_sqinds(exd, _arrsiz(exd))[1]	= 0;
		_sqharm(exd, _arrsiz(exd))[1]	= 0;
		_sqadto(exd, _arrsiz(exd))[1]	= 0;
		_sqtdso(exd, _arrsiz(exd))[1]	= 0;
										  
		_sqphsv(exd)					= _sqhstv(exd, _arrsiz(exd));
		_sqphsl(exd)					= _sqhstl(exd, _arrsiz(exd));
		_sqphss(exd)					= _sqhsts(exd, _arrsiz(exd));
		_sqpind(exd)					= _sqindv(exd, _arrsiz(exd));
		_sqpins(exd)					= _sqinds(exd, _arrsiz(exd));
		_sqparm(exd)					= _sqharm(exd, _arrsiz(exd));
		_sqparc(exd)					= _sqharc(exd, _arrsiz(exd));
		_sqpadto(exd)					= _sqadto(exd, _arrsiz(exd));
		_sqptdso(exd)					= _sqtdso(exd, _arrsiz(exd));
		_sqlcmax(exd)					= 100;
		_sqlcmin(exd)					= 2;
		_sqlcincr(exd)					= 1;
		_sqlctimeout(exd)				= 0;
		_sqlcnowait(exd)				= 0;

		sqlcxt(context, &sqlctx, exd, &sqlfpn);

		free(_cud(exd));
		free(exd);

		return SQLCA_ERROR_CODE;
	}

int ora_exd_commit_work(sql_context* context)
	{
		unsigned int	exdSz	= sqlexdBaseSize;
		exd_struct		exd		= malloc(exdSz);

		memset(exd, 0, exdSz);

		_sqladtp(exd)			= &sqladt;
		_sqltdsp(exd)			= &sqltds;

		_sqlvsn(exd)			= 13;
		_sqlest(exd)			= (unsigned char*)(unsigned char*)(&_sqlca);
		_sqlety(exd)			= 4352;

		_cud(exd)				= ora_get_cud_commit_work();
		_iters(exd)				= 1;
		_offset(exd)			= CUD_HEADER_OFFSET;
		_occurs(exd)			= 0;

		sqlcxt(context, &sqlctx, exd, &sqlfpn);

		free(_cud(exd));
		free(exd);

		return SQLCA_ERROR_CODE;
	}

int ora_exd_commit_work_release(sql_context* context)
	{
		unsigned int	exdSz	= sqlexdBaseSize;
		exd_struct		exd		= malloc(exdSz);

		memset(exd, 0, exdSz);

		_sqladtp(exd)			= &sqladt;
		_sqltdsp(exd)			= &sqltds;

		_sqlvsn(exd)			= 13;
		_sqlest(exd)			= (unsigned char*)(unsigned char*)(&_sqlca);
		_sqlety(exd)			= 4352;

		_cud(exd)				= ora_get_cud_commit_work_release();
		_iters(exd)				= 1;
		_offset(exd)			= CUD_HEADER_OFFSET;
		_occurs(exd)			= 0;

		sqlcxt(context, &sqlctx, exd, &sqlfpn);

		free(_cud(exd));
		free(exd);

		return SQLCA_ERROR_CODE;
	}

int ora_exd_open_cursor(short cursor_id, const char* stmt, sql_context* context)
	{
		unsigned int	exdSz	= sqlexdBaseSize;
		exd_struct		exd		= malloc(exdSz);

		memset(exd, 0, exdSz);

		_sqladtp(exd)			= &sqladt;
		_sqltdsp(exd)			= &sqltds;

		_sqlvsn(exd)			= 13;
		_sqlest(exd)			= (unsigned char*)(&_sqlca);
		_sqlety(exd)			= 4352;

		_cud(exd)				= ora_get_cud_open_cursor(cursor_id, (short)strlen(stmt));
		_stmt(exd)				= stmt;
		_iters(exd)				= 1;
		_offset(exd)			= CUD_HEADER_OFFSET;
		_selerr(exd)			= 1;
		_sqlpfmem(exd)			= 0;
		_occurs(exd)			= 0;
		_sqcmod(exd)			= 0;

		sqlcxt(context, &sqlctx, exd, &sqlfpn);

		free(_cud(exd));
		free(exd);

		return SQLCA_ERROR_CODE;
	}

ora_cortege* ora_open_cortege(unsigned int attribCount, ora_data_type* attribOutTypes)
	{
		ora_cortege* cortege	= malloc(sizeof(ora_cortege));

		cortege->length			= attribCount;
		cortege->values			= ORA_NULL;
		cortege->values_types	= malloc(attribCount * sizeof(ora_data_type));

		memcpy(cortege->values_types, attribOutTypes, sizeof(ora_data_type) * attribCount);

		return cortege;
	}

void ora_close_cortege(ora_cortege* cortege)
	{
		if(cortege->values != ORA_NULL)
			free(cortege->values);

		if(cortege->values_types != ORA_NULL)
			free(cortege->values_types);

		free(cortege);
	}

exd_struct ora_exd_fetch_init_cursor(short cursor_id, ora_cortege* cortegeInfo)
	{
		unsigned int	exdSz	= sqlexdBaseSize + (sqlexdParamSize * cortegeInfo->length);
		exd_struct		exd		= malloc(exdSz);

		memset(exd, 0, exdSz);

		_sqladtp(exd)			= &sqladt;
		_sqltdsp(exd)			= &sqltds;

		_sqlvsn(exd)			= 13;
		_arrsiz(exd)			= cortegeInfo->length;
		_sqlest(exd)			= (unsigned char*)(&_sqlca);
		_sqlety(exd)			= 4352;

		_cud(exd)				= ora_get_cud_fetch_into(cursor_id, cortegeInfo->length, cortegeInfo->values_types);
		_iters(exd)				= 1;
		_offset(exd)			= CUD_HEADER_OFFSET;
		_selerr(exd)			= 1;
		_sqlpfmem(exd)			= 0;
		_occurs(exd)			= 0;
		_sqfoff(exd)			= 0;
		_sqfmod(exd)			= 2;

		cortegeInfo->values		= malloc(sizeof(void*) * cortegeInfo->length);

		for(unsigned int index = 0; index < cortegeInfo->length; ++index)
			{
				cortegeInfo->values[index]			= malloc(ORA_TYPE_SIZE(cortegeInfo->values_types[index]));

				_sqhstv(exd, _arrsiz(exd))[index]	= cortegeInfo->values[index];
				_sqhstl(exd, _arrsiz(exd))[index]	= ORA_TYPE_SIZE(cortegeInfo->values_types[index]);
				_sqhsts(exd, _arrsiz(exd))[index]	= 0;
				_sqindv(exd, _arrsiz(exd))[index]	= 0;
				_sqinds(exd, _arrsiz(exd))[index]	= 0;
				_sqharm(exd, _arrsiz(exd))[index]	= 0;
				_sqadto(exd, _arrsiz(exd))[index]	= 0;
				_sqtdso(exd, _arrsiz(exd))[index]	= 0;
			}
		
		_sqphsv(exd)	= _sqhstv(exd, _arrsiz(exd));
		_sqphsl(exd)	= _sqhstl(exd, _arrsiz(exd));
		_sqphss(exd)	= _sqhsts(exd, _arrsiz(exd));
		_sqpind(exd)	= _sqindv(exd, _arrsiz(exd));
		_sqpins(exd)	= _sqinds(exd, _arrsiz(exd));
		_sqparm(exd)	= _sqharm(exd, _arrsiz(exd));
		_sqparc(exd)	= _sqharc(exd, _arrsiz(exd));
		_sqpadto(exd)	= _sqadto(exd, _arrsiz(exd));
		_sqptdso(exd)	= _sqtdso(exd, _arrsiz(exd));

		return exd;
	}

int ora_exd_fetch_cursor(exd_struct exd, sql_context* context)
	{
		sqlcxt(context, &sqlctx, exd, &sqlfpn);

		return SQLCA_ERROR_CODE;
	}

void ora_free_exd(exd_struct exd)
	{
		if(_cud(exd))
			free(_cud(exd));

		unsigned int	len = _arrsiz(exd);
		void*			curr;

		for(unsigned int index = 0; index < len; ++index)
			{
				curr = _sqhstv(exd, _arrsiz(exd))[index];

				if(curr)
					free(curr);
			}

		free(exd);
	}

int ora_exd_close_cursor(short cursor_id, sql_context* context)
	{
		unsigned int	exdSz	= sqlexdBaseSize;
		exd_struct		exd		= malloc(exdSz);

		memset(exd, 0, exdSz);

		_sqladtp(exd)			= &sqladt;
		_sqltdsp(exd)			= &sqltds;

		_sqlvsn(exd)			= 13;
		_sqlest(exd)			= (unsigned char*)(&_sqlca);
		_sqlety(exd)			= 4352;

		_cud(exd)				= ora_get_cud_close_cursor(cursor_id);
		_iters(exd)				= 1;
		_offset	(exd)			= CUD_HEADER_OFFSET;
		_occurs(exd)			= 0;

		sqlcxt(context, &sqlctx, exd, &sqlfpn);

		free(_cud(exd));
		free(exd);

		return SQLCA_ERROR_CODE;
	}

int ora_exd_create_table(const char* stmt, sql_context* context)
	{
		unsigned int	exdSz	= sqlexdBaseSize;
		exd_struct		exd		= malloc(exdSz);

		memset(exd, 0, exdSz);

		_sqladtp(exd)			= &sqladt;
		_sqltdsp(exd)			= &sqltds;

		_sqlvsn(exd)			= 13;
		_sqlest(exd)			= (unsigned char*)(&_sqlca);
		_sqlety(exd)			= 4352;

		_cud(exd)				= ora_get_cud_create_table((short)strlen(stmt));
		_stmt(exd)				= stmt;
		_iters(exd)				= 1;
		_offset(exd)			= CUD_HEADER_OFFSET;
		_occurs(exd)			= 0;

		sqlcxt(context, &sqlctx, exd, &sqlfpn);

		free(_cud(exd));
		free(exd);

		return SQLCA_ERROR_CODE;
	}

int ora_exd_drop_table(const char* stmt, sql_context* context)
	{
		unsigned int	exdSz	= sqlexdBaseSize;
		exd_struct		exd		= malloc(exdSz);

		memset(exd, 0, exdSz);

		_sqladtp(exd)			= &sqladt;
		_sqltdsp(exd)			= &sqltds;

		_sqlvsn(exd)			= 13;
		_sqlest(exd)			= (unsigned char*)(&_sqlca);
		_sqlety(exd)			= 4352;

		_cud(exd)				= ora_get_cud_drop_table((short)strlen(stmt));
		_stmt(exd)				= stmt;
		_iters(exd)				= 1;
		_offset(exd)			= CUD_HEADER_OFFSET;
		_occurs(exd)			= 0;

		sqlcxt(context, &sqlctx, exd, &sqlfpn);

		free(_cud(exd));
		free(exd);

		return SQLCA_ERROR_CODE;
	}

int ora_exd_alter_table(const char* stmt, sql_context* context)
	{
		unsigned int	exdSz	= sqlexdBaseSize;
		exd_struct		exd		= malloc(exdSz);

		memset(exd, 0, exdSz);

		_sqladtp(exd)			= &sqladt;
		_sqltdsp(exd)			= &sqltds;

		_sqlvsn(exd)			= 13;
		_sqlest(exd)			= (unsigned char*)(&_sqlca);
		_sqlety(exd)			= 4352;

		_cud(exd)				= ora_get_cud_alter_table((short)strlen(stmt));
		_stmt(exd)				= stmt;
		_iters(exd)				= 1;
		_offset(exd)			= CUD_HEADER_OFFSET;
		_occurs(exd)			= 0;

		sqlcxt(context, &sqlctx, exd, &sqlfpn);

		free(_cud(exd));
		free(exd);

		return SQLCA_ERROR_CODE;
	}

int ora_exd_update_table(const char* stmt, sql_context* context)
	{
		unsigned int	exdSz	= sqlexdBaseSize;
		exd_struct		exd		= malloc(exdSz);

		memset(exd, 0, exdSz);

		_sqladtp(exd)			= &sqladt;
		_sqltdsp(exd)			= &sqltds;

		_sqlvsn(exd)			= 13;
		_sqlest(exd)			= (unsigned char*)(unsigned char*)(&_sqlca);
		_sqlety(exd)			= 4352;

		_cud(exd)				= ora_get_cud_update_table((short)strlen(stmt));
		_stmt(exd)				= stmt;
		_iters(exd)				= 1;
		_offset(exd)			= CUD_HEADER_OFFSET;
		_occurs(exd)			= 0;

		sqlcxt(context, &sqlctx, exd, &sqlfpn);

		free(_cud(exd));
		free(exd);

		return SQLCA_ERROR_CODE;
	}

int ora_exd_insert_into(const char* stmt, sql_context* context)
	{
		unsigned int	exdSz	= sqlexdBaseSize;
		exd_struct		exd		= malloc(exdSz);

		memset(exd, 0, exdSz);

		_sqladtp(exd)			= &sqladt;
		_sqltdsp(exd)			= &sqltds;

		_sqlvsn(exd)			= 13;
		_sqlest(exd)			= (unsigned char*)(unsigned char*)(&_sqlca);
		_sqlety(exd)			= 4352;

		_cud(exd)				= ora_get_cud_insert_into((short)strlen(stmt));
		_stmt(exd)				= stmt;
		_iters(exd)				= 1;
		_offset(exd)			= CUD_HEADER_OFFSET;
		_occurs(exd)			= 0;

		sqlcxt(context, &sqlctx, exd, &sqlfpn);

		free(_cud(exd));
		free(exd);

		return SQLCA_ERROR_CODE;
	}

int ora_exd_delete_from(const char* stmt, sql_context* context)
	{
		unsigned int	exdSz	= sqlexdBaseSize;
		exd_struct		exd		= malloc(exdSz);

		memset(exd, 0, exdSz);

		_sqladtp(exd)			= &sqladt;
		_sqltdsp(exd)			= &sqltds;

		_sqlvsn(exd)			= 13;
		_sqlest(exd)			= (unsigned char*)(unsigned char*)(&_sqlca);
		_sqlety(exd)			= 4352;

		_cud(exd)				= ora_get_cud_delete_from((short)strlen(stmt));
		_stmt(exd)				= stmt;
		_iters(exd)				= 1;
		_offset(exd)			= CUD_HEADER_OFFSET;
		_occurs(exd)			= 0;

		sqlcxt(context, &sqlctx, exd, &sqlfpn);

		free(_cud(exd));
		free(exd);

		return SQLCA_ERROR_CODE;
	}