#include "OracleAPI.h"
#include <stdio.h>
#include <stdlib.h>

void main()
	{
		if(ora_exd_connect_identified("system", "masterkey", USER_MODE, ORA_NULL) != ORA_ERR_NO_ERROR)
			{
				printf(_sqlca.sqlerrm.sqlerrmc);
				system("pause>Nul");
				return;
			}

		if(ora_exd_open_cursor(1, "select table_name from user_tables", ORA_NULL) != ORA_ERR_NO_ERROR)
			{
				printf(_sqlca.sqlerrm.sqlerrmc);

				system("pause>Nul");
			}

		ora_data_type	types[1]	=	{
											ORA_CHARZ(30)
										};
		ora_cortege*	cortege		= ora_open_cortege(1, types);
		exd_struct		exd			= ora_exd_fetch_init_cursor(1, cortege);

		while(ora_exd_fetch_cursor(exd, ORA_NULL) == ORA_ERR_NO_ERROR)
			printf("%s\n", cortege->values[0]);

		if(SQLCA_ERROR_CODE != ORA_ERR_NOT_FOUND)
			printf(SQLCA_ERROR_MSG);

		if(ora_exd_close_cursor(1, ORA_NULL) != ORA_ERR_NO_ERROR)
			printf(SQLCA_ERROR_MSG);

		ora_free_exd(exd);
		ora_close_cortege(cortege);

		if(ora_exd_commit_work_release(ORA_NULL) != ORA_ERR_NO_ERROR)
			printf(SQLCA_ERROR_MSG);

		system("pause>Nul");
	}