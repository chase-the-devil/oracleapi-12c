/***	Taken from Oracle Pro*C precompiler (from Oracle 12c)		***/

/* Result Sets Interface */

#ifndef SQL_CRSR

	#define SQL_CRSR

	struct sql_cursor
		{
			unsigned int	curocn;
			void*			ptr1;
			void*			ptr2;
			unsigned int	magic;
		};

  typedef struct sql_cursor sql_cursor;
  typedef struct sql_cursor SQL_CURSOR;

#endif /* SQL_CRSR */

/* Thread Safety */

typedef void* sql_context;
typedef void* SQL_CONTEXT;

/* Object support */

struct sqltvn
	{
		unsigned char*	tvnvsn; 
		unsigned short	tvnvsnl; 
		unsigned char*	tvnnm;
		unsigned short	tvnnml; 
		unsigned char*	tvnsnm;
		unsigned short	tvnsnml;
	};

typedef struct sqltvn sqltvn;

struct sqladts
	{
		unsigned int	adtvsn; 
		unsigned short	adtmode; 
		unsigned short	adtnum;  
		sqltvn			adttvn[1];       
	};

typedef struct sqladts sqladts;

static struct sqladts sqladt =	{	1, 1, 0	};

/* Binding to PL/SQL Records */

struct sqltdss
	{
		unsigned int	tdsvsn; 
		unsigned short	tdsnum; 
		unsigned char*	tdsval[1]; 
	};

typedef struct sqltdss sqltdss;

static struct sqltdss sqltds =	{	1, 0	};

/* File name & Package Name */

struct sqlcxp
	{
		unsigned short	fillen;
		char			filnam[21];
	};

//edited

static struct sqlcxp sqlfpn =	{
									20,
									"External Oracle Call"
								};

static unsigned int sqlctx = 314707;

static struct sqlexd 
	{
		unsigned int	sqlvsn;
		unsigned int	arrsiz;
		unsigned int	iters;
		unsigned int	offset;
		unsigned short	selerr;
		unsigned short	sqlety;
		unsigned int	occurs;
		const short*	cud;
		unsigned char*	sqlest;
		const char*		stmt;
		sqladts*		sqladtp;
		sqltdss*		sqltdsp;
		void**			sqphsv;
		unsigned int*	sqphsl;
		int*			sqphss;
		void**			sqpind;
		int*			sqpins;
		unsigned int*	sqparm;
		unsigned int**	sqparc;
		unsigned short*	sqpadto;
		unsigned short*	sqptdso;
		unsigned int	sqlcmax;
		unsigned int	sqlcmin;
		unsigned int	sqlcincr;
		unsigned int	sqlctimeout;
		unsigned int	sqlcnowait;
		int				sqfoff;
		unsigned int	sqcmod;
		unsigned int	sqfmod;
		unsigned int	sqlpfmem;
		
		void*			sqhstv[1];
		unsigned int	sqhstl[1];
		int				sqhsts[1];
		void*			sqindv[1];
		int				sqinds[1];
		unsigned int	sqharm[1];
		unsigned int*	sqharc[1];
		unsigned short  sqadto[1];
		unsigned short  sqtdso[1];
	} sqlstm = {13,4};

/* SQLLIB Prototypes */

extern void sqlcxt	(void **, unsigned int*, void*, const struct sqlcxp*		);
extern void sqlcx2t	(void **, unsigned int*, void*, const struct sqlcxp*		);
extern void sqlbuft	(void **, char *											);
extern void sqlgs2t	(void **, char *											);
extern void sqlorat	(void **, unsigned int *, void *							);

/* Forms Interface */

static const int IAPSUCC = 0;
static const int IAPFAIL = 1403;
static const int IAPFTL  = 535;

extern void sqliem	(unsigned char*, signed int*								);

/*
 * $Header: precomp/public/sqlca.h /osds/1 2011/08/02 23:12:16 pnayak Exp $ sqlca.h 
 */

/* Copyright (c) 1985, 2003, Oracle Corporation.  All rights reserved.  */
 
/*
NAME
  SQLCA : SQL Communications Area.
FUNCTION
  Contains no code. Oracle fills in the SQLCA with status info
  during the execution of a SQL stmt.
NOTES
  **************************************************************
  ***                                                        ***
  *** This file is SOSD.  Porters must change the data types ***
  *** appropriately on their platform.  See notes/pcport.doc ***
  *** for more information.                                  ***
  ***                                                        ***
  **************************************************************

  If the symbol SQLCA_STORAGE_CLASS is defined, then the SQLCA
  will be defined to have this storage class. For example:
 
    #define SQLCA_STORAGE_CLASS extern
 
  will define the SQLCA as an extern.
 
  If the symbol SQLCA_INIT is defined, then the SQLCA will be
  statically initialized. Although this is not necessary in order
  to use the SQLCA, it is a good pgming practice not to have
  unitialized variables. However, some C compilers/OS's don't
  allow automatic variables to be init'd in this manner. Therefore,
  if you are INCLUDE'ing the SQLCA in a place where it would be
  an automatic AND your C compiler/OS doesn't allow this style
  of initialization, then SQLCA_INIT should be left undefined --
  all others can define SQLCA_INIT if they wish.

  If the symbol SQLCA_NONE is defined, then the SQLCA variable will
  not be defined at all.  The symbol SQLCA_NONE should not be defined
  in source modules that have embedded SQL.  However, source modules
  that have no embedded SQL, but need to manipulate a sqlca struct
  passed in as a parameter, can set the SQLCA_NONE symbol to avoid
  creation of an extraneous sqlca variable.
 
MODIFIED
    lvbcheng   07/31/98 -  long to int
    jbasu      12/12/94 -  Bug 217878: note this is an SOSD file
    losborne   08/11/92 -  No sqlca var if SQLCA_NONE macro set 
  Clare      12/06/84 - Ch SQLCA to not be an extern.
  Clare      10/21/85 - Add initialization.
  Bradbury   01/05/86 - Only initialize when SQLCA_INIT set
  Clare      06/12/86 - Add SQLCA_STORAGE_CLASS option.
*/
 
#ifndef SQLCA

	#define SQLCA 1
 
	struct sqlca
		{
			char	sqlcaid[8];
			int		sqlabc;
			int		sqlcode;
			
			struct
				{
					unsigned short	sqlerrml;
					char			sqlerrmc[70];
				}	sqlerrm;

			char	sqlerrp	[8];
			int		sqlerrd	[6];
			char	sqlwarn	[8];
			char	sqlext	[8];
		};

	#ifndef SQLCA_NONE
	
		#ifdef   SQLCA_STORAGE_CLASS
			SQLCA_STORAGE_CLASS struct sqlca sqlca
		#else
			struct sqlca _sqlca
		#endif
	 
		#ifdef  SQLCA_INIT
			=	{
					{'S', 'Q', 'L', 'C', 'A', ' ', ' ', ' '	},
					sizeof(struct sqlca),
					0,
					{ 0, {0}								},
					{'N', 'O', 'T', ' ', 'S', 'E', 'T', ' '	},
					{0, 0, 0, 0, 0, 0						},
					{0, 0, 0, 0, 0, 0, 0, 0					},
					{0, 0, 0, 0, 0, 0, 0, 0					}
				}
		#endif
		;
	#endif
 
#endif
 
/* end SQLCA */

//******************************************************
//***				Made by Mr-Cas					****
//******* Oracle API (Tested on Oracle 12c)	************
//******************************************************

#define		ORA_NULL				((void*)(0))

//sqlca fields

#define		SQLCA_AFFECTED_ROWS		_sqlca.sqlerrd[2]
#define		SQLCA_ERROR_CODE		_sqlca.sqlcode
#define		SQLCA_ERROR_MSG			_sqlca.sqlerrm.sqlerrmc
#define		SQLCA_ERROR_MSG_LEN		70

#ifdef	_WIN64
	#define		HANDLE_INT			unsigned __int64
#elif	_WIN32
	#define		HANDLE_INT			unsigned __int32
#endif

#define		offset_sqlvsn			(HANDLE_INT)(0)
#define		offset_arrsiz			offset_sqlvsn			+ (HANDLE_INT)(sizeof(unsigned int		))
#define		offset_iters			offset_arrsiz			+ (HANDLE_INT)(sizeof(unsigned int		))
#define		offset_offset			offset_iters			+ (HANDLE_INT)(sizeof(unsigned int		))
#define		offset_selerr			offset_offset			+ (HANDLE_INT)(sizeof(unsigned int		))
#define		offset_sqlety			offset_selerr			+ (HANDLE_INT)(sizeof(unsigned short	))
#define		offset_occurs			offset_sqlety			+ (HANDLE_INT)(sizeof(unsigned short	))
#define		offset_cud				offset_occurs			+ (HANDLE_INT)(sizeof(unsigned int		))
#define		offset_sqlest			offset_cud				+ (HANDLE_INT)(sizeof(const short*		))
#define		offset_stmt				offset_sqlest			+ (HANDLE_INT)(sizeof(unsigned char*	))
#define		offset_sqladtp			offset_stmt				+ (HANDLE_INT)(sizeof(const char*		))
#define		offset_sqltdsp			offset_sqladtp			+ (HANDLE_INT)(sizeof(sqladts*			))
#define		offset_sqphsv			offset_sqltdsp			+ (HANDLE_INT)(sizeof(sqltdss*			))
#define		offset_sqphsl			offset_sqphsv			+ (HANDLE_INT)(sizeof(void**			))
#define		offset_sqphss			offset_sqphsl			+ (HANDLE_INT)(sizeof(unsigned int*		))
#define		offset_sqpind			offset_sqphss			+ (HANDLE_INT)(sizeof(int*				))
#define		offset_sqpins			offset_sqpind			+ (HANDLE_INT)(sizeof(void**			))
#define		offset_sqparm			offset_sqpins			+ (HANDLE_INT)(sizeof(int*				))
#define		offset_sqparc			offset_sqparm			+ (HANDLE_INT)(sizeof(unsigned int*		))
#define		offset_sqpadto			offset_sqparc			+ (HANDLE_INT)(sizeof(unsigned int**	))
#define		offset_sqptdso			offset_sqpadto			+ (HANDLE_INT)(sizeof(unsigned short*	))	
#define		offset_sqlcmax			offset_sqptdso			+ (HANDLE_INT)(sizeof(unsigned short*	))	
#define		offset_sqlcmin			offset_sqlcmax			+ (HANDLE_INT)(sizeof(unsigned int		))
#define		offset_sqlcincr			offset_sqlcmin			+ (HANDLE_INT)(sizeof(unsigned int		))
#define		offset_sqlctimeout		offset_sqlcincr			+ (HANDLE_INT)(sizeof(unsigned int		))
#define		offset_sqlcnowait		offset_sqlctimeout		+ (HANDLE_INT)(sizeof(unsigned int		))
#define		offset_sqfoff			offset_sqlcnowait		+ (HANDLE_INT)(sizeof(unsigned int		))
#define		offset_sqcmod			offset_sqfoff			+ (HANDLE_INT)(sizeof(int				))	
#define		offset_sqfmod			offset_sqcmod			+ (HANDLE_INT)(sizeof(unsigned int		))
#define		offset_sqlpfmem			offset_sqfmod			+ (HANDLE_INT)(sizeof(unsigned int		))
#define		offset_params			offset_sqlpfmem			+ (HANDLE_INT)(sizeof(unsigned int		))

#define		_sqlvsn(inParam)		*((unsigned int*  	)((HANDLE_INT)(inParam) + offset_sqlvsn		))
#define		_arrsiz(inParam)		*((unsigned int*  	)((HANDLE_INT)(inParam) + offset_arrsiz		))
#define		_iters(inParam)			*((unsigned int*  	)((HANDLE_INT)(inParam) + offset_iters		))
#define		_offset(inParam)		*((unsigned int*  	)((HANDLE_INT)(inParam) + offset_offset		))
#define		_selerr(inParam)		*((unsigned short*	)((HANDLE_INT)(inParam) + offset_selerr		))
#define		_sqlety(inParam)		*((unsigned short*	)((HANDLE_INT)(inParam) + offset_sqlety		))
#define		_occurs(inParam)		*((unsigned int*  	)((HANDLE_INT)(inParam) + offset_occurs		))
#define		_cud(inParam)			*((short**  		)((HANDLE_INT)(inParam) + offset_cud		))
#define		_sqlest(inParam)		*((unsigned char**	)((HANDLE_INT)(inParam) + offset_sqlest		))
#define		_stmt(inParam)			*((const char**	  	)((HANDLE_INT)(inParam) + offset_stmt		))
#define		_sqladtp(inParam)		*((sqladts**	  	)((HANDLE_INT)(inParam) + offset_sqladtp	))
#define		_sqltdsp(inParam)		*((sqltdss**	  	)((HANDLE_INT)(inParam) + offset_sqltdsp	))
#define		_sqphsv(inParam)		*((void***			)((HANDLE_INT)(inParam) + offset_sqphsv		))
#define		_sqphsl(inParam)		*((unsigned int** 	)((HANDLE_INT)(inParam) + offset_sqphsl		))
#define		_sqphss(inParam)		*((int**			)((HANDLE_INT)(inParam) + offset_sqphss		))
#define		_sqpind(inParam)		*((void***			)((HANDLE_INT)(inParam) + offset_sqpind		))
#define		_sqpins(inParam)		*((int**			)((HANDLE_INT)(inParam) + offset_sqpins		))
#define		_sqparm(inParam)		*((unsigned int**	)((HANDLE_INT)(inParam) + offset_sqparm		))
#define		_sqparc(inParam)		*((unsigned int***	)((HANDLE_INT)(inParam) + offset_sqparc		))
#define		_sqpadto(inParam)		*((unsigned short**	)((HANDLE_INT)(inParam) + offset_sqpadto	))
#define		_sqptdso(inParam)		*((unsigned short**	)((HANDLE_INT)(inParam) + offset_sqptdso	))
#define		_sqlcmax(inParam)		*((unsigned int*   	)((HANDLE_INT)(inParam) + offset_sqlcmax	))
#define		_sqlcmin(inParam)		*((unsigned int*   	)((HANDLE_INT)(inParam) + offset_sqlcmin	))
#define		_sqlcincr(inParam)		*((unsigned int*   	)((HANDLE_INT)(inParam) + offset_sqlcincr	))
#define		_sqlctimeout(inParam)	*((unsigned int*   	)((HANDLE_INT)(inParam) + offset_sqlctimeout))
#define		_sqlcnowait(inParam)	*((unsigned int*   	)((HANDLE_INT)(inParam) + offset_sqlcnowait	))
#define		_sqfoff(inParam)		*((int*				)((HANDLE_INT)(inParam) + offset_sqfoff		))
#define		_sqcmod(inParam)		*((unsigned int*   	)((HANDLE_INT)(inParam) + offset_sqcmod		))
#define		_sqfmod(inParam)		*((unsigned int*   	)((HANDLE_INT)(inParam) + offset_sqfmod		))
#define		_sqlpfmem(inParam)		*((unsigned int*   	)((HANDLE_INT)(inParam) + offset_sqlpfmem	))

#ifdef	_WIN64
	#define		offset_sqhstv(inParamCount)		(offset_params + (HANDLE_INT)(4)) //memory allignment
#elif	_WIN32
	#define		offset_sqhstv(inParamCount)		offset_params
#endif

#define		offset_sqhstl(inParamCount)		offset_sqhstv(inParamCount) + ((HANDLE_INT)(inParamCount) * (HANDLE_INT)(sizeof(void*			)))
#define		offset_sqhsts(inParamCount)		offset_sqhstl(inParamCount) + ((HANDLE_INT)(inParamCount) * (HANDLE_INT)(sizeof(unsigned int	)))
#define		offset_sqindv(inParamCount)		offset_sqhsts(inParamCount) + ((HANDLE_INT)(inParamCount) * (HANDLE_INT)(sizeof(int				)))
#define		offset_sqinds(inParamCount)		offset_sqindv(inParamCount) + ((HANDLE_INT)(inParamCount) * (HANDLE_INT)(sizeof(void*			)))
#define		offset_sqharm(inParamCount)		offset_sqinds(inParamCount) + ((HANDLE_INT)(inParamCount) * (HANDLE_INT)(sizeof(int				)))
#define		offset_sqharc(inParamCount)		offset_sqharm(inParamCount) + ((HANDLE_INT)(inParamCount) * (HANDLE_INT)(sizeof(unsigned int	)))
#define		offset_sqadto(inParamCount)		offset_sqharc(inParamCount) + ((HANDLE_INT)(inParamCount) * (HANDLE_INT)(sizeof(unsigned int*	)))
#define		offset_sqtdso(inParamCount)		offset_sqadto(inParamCount) + ((HANDLE_INT)(inParamCount) * (HANDLE_INT)(sizeof(unsigned short	)))

#define		_sqhstv(inBase, inParamCount)	((void**		 )((HANDLE_INT)(inBase) + offset_sqhstv(inParamCount)	))
#define		_sqhstl(inBase, inParamCount)	((unsigned int*	 )((HANDLE_INT)(inBase) + offset_sqhstl(inParamCount)	))
#define		_sqhsts(inBase, inParamCount)	((int*			 )((HANDLE_INT)(inBase) + offset_sqhsts(inParamCount)	))
#define		_sqindv(inBase, inParamCount)	((void**		 )((HANDLE_INT)(inBase) + offset_sqindv(inParamCount)	))
#define		_sqinds(inBase, inParamCount)	((int*			 )((HANDLE_INT)(inBase) + offset_sqinds(inParamCount)	))
#define		_sqharm(inBase, inParamCount)	((unsigned int*	 )((HANDLE_INT)(inBase) + offset_sqharm(inParamCount)	))
#define		_sqharc(inBase, inParamCount)	((unsigned int** )((HANDLE_INT)(inBase) + offset_sqharc(inParamCount)	))
#define		_sqadto(inBase, inParamCount)	((unsigned short*)((HANDLE_INT)(inBase) + offset_sqadto(inParamCount)	))
#define		_sqtdso(inBase, inParamCount)	((unsigned short*)((HANDLE_INT)(inBase) + offset_sqtdso(inParamCount)	))

static const unsigned int sqlexdBaseSize = 
	13	*	sizeof(unsigned int		) +
	2	*	sizeof(unsigned short	) +
			sizeof(int				) +
	14	*	sizeof(void*			);

static const unsigned int sqlexdParamSize =
	3	*	sizeof(void*			) +
	2	*	sizeof(unsigned int		) +
	2	*	sizeof(int				) +
	2	*	sizeof(unsigned short	)

#ifdef _WIN64
	+ 4;		//memory allignment
#elif _WIN32
	;
#endif

#define		ORA_ERR_NO_ERROR						0
#define		ORA_ERR_NOT_FOUND						1403

typedef 
	unsigned __int64	ora_data_type;

typedef
	unsigned __int32	ora_data_type_size;

typedef
	void*				exd_struct;

//***		data types

#define		ORA_TYPE_VARCHAR2						(ora_data_type)(0x0000000100000000	)
#define		ORA_TYPE_NUMBER							(ora_data_type)(0x0000000200000000	)
#define		ORA_TYPE_SIGNED_INTEGER					(ora_data_type)(0x0000000400000000	)
#define		ORA_TYPE_FLOAT							(ora_data_type)(0x0000000800000000	)
#define		ORA_TYPE_STRING							(ora_data_type)(0x0000001000000000	)
#define		ORA_TYPE_VARNUM							(ora_data_type)(0x0000002000000000	)
#define		ORA_TYPE_LONG							(ora_data_type)(0x0000004000000000	)
#define		ORA_TYPE_VARCHAR						(ora_data_type)(0x0000008000000000	)
#define		ORA_TYPE_DATE							(ora_data_type)(0x0000010000000000	)
#define		ORA_TYPE_VARRAW							(ora_data_type)(0x0000020000000000	)
#define		ORA_TYPE_BINARY_FLOAT					(ora_data_type)(0x0000040000000000	)
#define		ORA_TYPE_BINARY_DOUBLE					(ora_data_type)(0x0000080000000000	)
#define		ORA_TYPE_RAW							(ora_data_type)(0x0000100000000000	)
#define		ORA_TYPE_LONG_RAW						(ora_data_type)(0x0000200000000000	)
#define		ORA_TYPE_UNSIGNED_INTEGER				(ora_data_type)(0x0000400000000000	)
#define		ORA_TYPE_LONG_VARCHAR					(ora_data_type)(0x0000800000000000	)
#define		ORA_TYPE_LONG_VARRAW					(ora_data_type)(0x0001000000000000	)
#define		ORA_TYPE_CHAR							(ora_data_type)(0x0002000000000000	)
#define		ORA_TYPE_CHARZ							(ora_data_type)(0x0004000000000000	)

#define		ORA_MAX_SIZE_VARCHAR2					(ora_data_type)(0x0000000000000fa0	)
#define		ORA_MAX_SIZE_NUMBER						(ora_data_type)(0x0000000000000015	)
#define		ORA_MAX_SIZE_SIGNED_INTEGER				(ora_data_type)(sizeof(__int64)		)
#define		ORA_MAX_SIZE_FLOAT						(ora_data_type)(sizeof(double)		)
#define		ORA_MAX_SIZE_STRING						(ora_data_type)(0x0000000000000f9f	)
#define		ORA_MAX_SIZE_VARNUM						(ora_data_type)(0x0000000000000016	)
#define		ORA_MAX_SIZE_LONG						(ora_data_type)(0x000000007fffffff	)
#define		ORA_MAX_SIZE_VARCHAR					(ora_data_type)(0x000000000000fffd	)
#define		ORA_MAX_SIZE_DATE						(ora_data_type)(0x0000000000000007	)
#define		ORA_MAX_SIZE_VARRAW						(ora_data_type)(0x000000000000fffd	)
#define		ORA_MAX_SIZE_BINARY_FLOAT				(ora_data_type)(sizeof(float)		)
#define		ORA_MAX_SIZE_BINARY_DOUBLE				(ora_data_type)(sizeof(double)		)
#define		ORA_MAX_SIZE_RAW						(ora_data_type)(0x00000000000007d0	)
#define		ORA_MAX_SIZE_LONGRAW					(ora_data_type)(0x000000007fffffff	)
#define		ORA_MAX_SIZE_UNSIGNED					(ora_data_type)(sizeof(unsigned int))
#define		ORA_MAX_SIZE_LONG_VARCHAR				(ora_data_type)(0x000000007ffffffb	)
#define		ORA_MAX_SIZE_LONG_VARRAW				(ora_data_type)(0x000000007ffffffb	)
#define		ORA_MAX_SIZE_CHAR						(ora_data_type)(0x00000000000007d0	)
#define		ORA_MAX_SIZE_CHARZ						(ora_data_type)(0x00000000000007d0	)

//Input size various:
//1...ORA_MAX_SIZE_VARCHAR2
#define		ORA_VARCHAR2(inSize)					ORA_TYPE_VARCHAR2					|	(ora_data_type)(inSize					)			

#define		ORA_NUMBER								ORA_TYPE_NUMBER						|	(ora_data_type)(0x0000000000000015		)

//Input size one of follows:
//sizeof(char), sizeof(short), sizeof(int), sizeof(long), sizeof(long long)
//sizeof(__int8), sizeof(__int16), sizeof(__int32), sizeof(__int64)
#define		ORA_SIGNED_INTEGER(inSize)				ORA_TYPE_SIGNED_INTEGER				|	(ora_data_type)(inSize					)

//Input size one of follows:
//sizeof(float), sizeof(double)
#define		ORA_FLOAT(inSize)						ORA_TYPE_FLOAT						|	(ora_data_type)(inSize					)

//Input size various:
//1...ORA_MAX_SIZE_STRING
#define		ORA_STRING(inSize)						ORA_TYPE_STRING						|	(ora_data_type)(inSize + sizeof(char)	)

#define		ORA_VARNUM								ORA_TYPE_VARNUM						|	(ora_data_type)(0x0000000000000016		)

//Input size various:
//1...ORA_MAX_SIZE_LONG
#define		ORA_LONG(inSize)						ORA_TYPE_LONG						|	(ora_data_type)(inSize					)

//Input size various:
//1...ORA_MAX_SIZE_VARCHAR
#define		ORA_VARCHAR(inSize)						ORA_TYPE_VARCHAR					|	(ora_data_type)(inSize + sizeof(short)	)

#define		ORA_DATE								ORA_TYPE_DATE						|	(ora_data_type)(0x0000000000000007		)

//Input size various:
//1...ORA_MAX_SIZE_VARRAW
#define		ORA_VARRAW(inSize)						ORA_TYPE_VARRAW						|	(ora_data_type)(inSize + sizeof(short)	)

#define		ORA_BINARY_FLOAT						ORA_TYPE_BINARY_FLOAT				|	(ora_data_type)(sizeof(float)			)

#define		ORA_BINARY_DOUBLE						ORA_TYPE_BINARY_DOUBLE				|	(ora_data_type)(sizeof(double)			)

//Input size various:
//1...ORA_MAX_SIZE_RAW
#define		ORA_RAW(inSize)							ORA_TYPE_RAW						|	(ora_data_type)(inSize					)

//Input size various:
//1...ORA_MAX_SIZE_LONG_RAW
#define		ORA_LONG_RAW(inSize)					ORA_TYPE_LONG_RAW					|	(ora_data_type)(inSize					)

#define		ORA_UNSIGNED_INTEGER					ORA_TYPE_UNSIGNED_INTEGER			|	(ora_data_type)(sizeof(unsigned int)	)

//Input size various:
//1...ORA_MAX_SIZE_LONG_VARCHAR
#define		ORA_LONG_VARCHAR(inSize)				ORA_TYPE_LONG_VARCHAR				|	(ora_data_type)(inSize + sizeof(int)	)

//Input size various:
//1...ORA_MAX_SIZE_LONG_VARRAW
#define		ORA_LONG_VARRAW(inSize)					ORA_TYPE_LONG_VARRAW				|	(ora_data_type)(inSize + sizeof(int)	)

//Input size various:
//1...ORA_MAX_SIZE_CHAR
#define		ORA_CHAR(inSize)						ORA_TYPE_CHAR						|	(ora_data_type)(inSize					)

//Input size various:
//1...ORA_MAX_SIZE_CHARZ
#define		ORA_CHARZ(inSize)						ORA_TYPE_CHARZ						|	(ora_data_type)(inSize + sizeof(char)	)

#define		ORA_TYPE_SIZE(inType)					(ora_data_type_size)(inType)

//			define this when you fetch out values from cursor

#define		C_TYPE_VARCHAR2							(short)(0x001	)
#define		C_TYPE_NUMBER							(short)(0x002	)
#define		C_TYPE_SIGNED_INTEGER					(short)(0x003	)
#define		C_TYPE_FLOAT							(short)(0x004	)
#define		C_TYPE_NULL_TERMINATED_STRING			(short)(0x005	)
#define		C_TYPE_VARNUM							(short)(0x006	)
#define		C_TYPE_LONG								(short)(0x008	)
#define		C_TYPE_VARCHAR							(short)(0x009	)
#define		C_TYPE_DATE								(short)(0x00c	)
#define		C_TYPE_VARRAW							(short)(0x00f	)
#define		C_TYPE_BINARY_FLOAT						(short)(0x015	)
#define		C_TYPE_BINARY_DOUBLE					(short)(0x016	)
#define		C_TYPE_RAW								(short)(0x017	)
#define		C_TYPE_LONG_RAW							(short)(0x018	)
#define		C_TYPE_UNSIGNED_INTEGER					(short)(0x044	)
#define		C_TYPE_LONG_VARCHAR						(short)(0x05e	)
#define		C_TYPE_LONG_VARRAW						(short)(0x05f	)
#define		C_TYPE_CHAR								(short)(0x060	)
#define		C_TYPE_CHARZ							(short)(0x061	)

#define		NATIVE_TYPE_INT8						(unsigned __int8)(0x00)
#define		NATIVE_TYPE_INT16						(unsigned __int8)(0x01)
#define		NATIVE_TYPE_INT32						(unsigned __int8)(0x02)
#define		NATIVE_TYPE_INT64						(unsigned __int8)(0x03)
#define		NATIVE_TYPE_NULL_TERMINATED_STRING		(unsigned __int8)(0x05)
#define		NATIVE_TYPE_VARCHAR						(unsigned __int8)(0x04)
#define		NATIVE_TYPE_LONG_VARCHAR				(unsigned __int8)(0x06)
#define		NATIVE_TYPE_VARRAW						(unsigned __int8)(0x07)
#define		NATIVE_TYPE_LONG_VARRAW					(unsigned __int8)(0x08)
#define		NATIVE_TYPE_STRING						(unsigned __int8)(0x09)
#define		NATIVE_TYPE_BYTE_ARRAY					(unsigned __int8)(0x0a)
#define		NATIVE_TYPE_VARNUM						(unsigned __int8)(0x0b)
#define		NATIVE_TYPE_DATE						(unsigned __int8)(0x0c)
#define		NATIVE_TYPE_FLOAT						(unsigned __int8)(0x0d)
#define		NATIVE_TYPE_DOUBLE						(unsigned __int8)(0x0e)
#define		NATIVE_TYPE_UNSIGNED_INT				(unsigned __int8)(0x0f)

//			cud 

#define		CUD_HEADER_OFFSET						5
#define		CUD_HEADER								13,4130,178,0,0

#define		CUD_CONNECT_LENGTH						(CUD_HEADER_OFFSET + 31)
#define		CUD_CONNECT								0,0,0,0,0,27,1,0,0,4,4,0,1,0,1,97,0,0,1,10,0,0,1,10,0,0,1,10,0,0

#define		CUD_CONNECT_IDENTIFIED_LENGTH			(CUD_HEADER_OFFSET + 31)
#define		CUD_CONNECT_IDENTIFIED					0,0,0,0,0,27,1,0,0,4,4,0,1,0,1,97,0,0,1,97,0,0,1,10,0,0,1,10,0,0

#define		CUD_COMMIT_WORK_LENGTH					(CUD_HEADER_OFFSET + 15)
#define		CUD_COMMIT_WORK							0,0,1,0,0,29,1,0,0,0,0,0,1,0

#define		CUD_COMMIT_WORK_RELEASE_LENGTH			(CUD_HEADER_OFFSET + 15)
#define		CUD_COMMIT_WORK_RELEASE					0,0,1,2,0,30,1,0,0,0,0,0,1,0

#define		CUD_ALTER_SESSION_LENGTH				(CUD_HEADER_OFFSET + 15)
#define		CUD_ALTER_SESSION						0,0,1,0,0,1,1,0,0,0,0,0,1,0

#define		CUD_CREATE_USER_LENGTH					(CUD_HEADER_OFFSET + 15)
#define		CUD_CREATE_USER							0,0,1,0,0,44,1,0,0,0,0,0,1,0

#define		CUD_DROP_USER_LENGTH					(CUD_HEADER_OFFSET + 15)
#define		CUD_DROP_USER							0,0,1,0,0,1,1,0,0,0,0,0,1,0

#define		CUD_GRANT_USER_LENGTH					(CUD_HEADER_OFFSET + 15)
#define		CUD_GRANT_USER							0,0,1,0,0,1,1,0,0,0,0,0,1,0

#define		CUD_OPEN_CURSOR_LENGTH					(CUD_HEADER_OFFSET + 15)
#define		CUD_OPEN_CURSOR							0,0,0,0,0,9,2,0,0,0,0,0,1,0

#define		CUD_CLOSE_CURSOR_LENGTH					(CUD_HEADER_OFFSET + 15)
#define		CUD_CLOSE_CURSOR						0,0,0,0,0,15,1,0,0,0,0,0,1,0

#define		CUD_FETCH_INTO_LENGTH					(CUD_HEADER_OFFSET + 15)
#define		CUD_FETCH_INTO							0,0,0,0,0,13,1,0,0,0,0,0,1,0

#define		CUD_CREATE_TABLE_LENGTH					(CUD_HEADER_OFFSET + 15)
#define		CUD_CREATE_TABLE						0,0,1,0,0,44,1,0,0,0,0,0,1,0

#define		CUD_DROP_TABLE_LENGTH					(CUD_HEADER_OFFSET + 15)
#define		CUD_DROP_TABLE							0,0,1,0,0,1,1,0,0,0,0,0,1,0

#define		CUD_ALTER_TABLE_LENGTH					(CUD_HEADER_OFFSET + 15)
#define		CUD_ALTER_TABLE							0,0,1,0,0,1,1,0,0,0,0,0,1,0

#define		CUD_UPDATE_TABLE_LENGTH					(CUD_HEADER_OFFSET + 15)
#define		CUD_UPDATE_TABLE						0,0,1,0,0,5,1,0,0,0,0,0,1,0

#define		CUD_INSERT_INTO_LENGTH					(CUD_HEADER_OFFSET + 15)
#define		CUD_INSERT_INTO							0,0,1,0,0,3,1,0,0,0,0,0,1,0

#define		CUD_DELETE_FROM_LENGTH					(CUD_HEADER_OFFSET + 15)
#define		CUD_DELETE_FROM							0,0,1,0,0,2,1,0,0,0,0,0,1,0

#define		CUD_ENABLE_THREADS_LENGTH				(CUD_HEADER_OFFSET + 15)
#define		CUD_ENABLE_THREADS						0,0,0,0,0,60,1,0,0,0,0,0,1,0

#define		CUD_CREATE_CONTEXT_LENGTH				(CUD_HEADER_OFFSET + 19)
#define		CUD_CREATE_CONTEXT						0,0,0,0,0,58,1,0,0,1,1,0,1,0,3,109,0,0

#define		CUD_FREE_CONTEXT_LENGTH					(CUD_HEADER_OFFSET + 19)
#define		CUD_FREE_CONTEXT						0,0,0,0,0,59,1,0,0,1,1,0,1,0,3,109,0,0

//

#define		OPEN_CURSOR_ID_INDEX					(CUD_HEADER_OFFSET + 3	)
#define		OPEN_CURSOR_STMT_LEN_INDEX				(CUD_HEADER_OFFSET + 4	)
													
#define		CLOSE_CURSOR_ID_INDEX					(CUD_HEADER_OFFSET + 3	)
													
#define		FETCH_INTO_CURSOR_ID_INDEX				(CUD_HEADER_OFFSET + 3	)
#define		FETCH_INTO_PARAM_COUNT_INDEX			(CUD_HEADER_OFFSET + 10	)
#define		FETCH_INTO_PARAMS_INDEX					(CUD_HEADER_OFFSET + 15	)

#define		CREATE_TABLE_STMT_LEN_INDEX				(CUD_HEADER_OFFSET + 4	)
#define		CREATE_TABLE_ID_INDEX					(CUD_HEADER_OFFSET + 3	)
													
#define		DROP_TABLE_STMT_LEN_INDEX				(CUD_HEADER_OFFSET + 4	)
#define		DROP_TABLE_ID_INDEX						(CUD_HEADER_OFFSET + 3	)
													
#define		ALTER_TABLE_STMT_LEN_INDEX				(CUD_HEADER_OFFSET + 4	)
#define		ALTER_TABLE_ID_INDEX					(CUD_HEADER_OFFSET + 3	)
													
#define		UPDATE_TABLE_STMT_LEN_INDEX				(CUD_HEADER_OFFSET + 4	)
#define		UPDATE_TABLE_ID_INDEX					(CUD_HEADER_OFFSET + 3	)
													
#define		INSERT_INTO_STMT_LEN_INDEX				(CUD_HEADER_OFFSET + 4	)
#define		INSERT_INTO_ID_INDEX					(CUD_HEADER_OFFSET + 3	)
													
#define		DELETE_FROM_STMT_LEN_INDEX				(CUD_HEADER_OFFSET + 4	)
#define		DELETE_FROM_ID_INDEX					(CUD_HEADER_OFFSET + 3	)
													
#define		CREATE_USER_STMT_LEN_INDEX				(CUD_HEADER_OFFSET + 4	)
#define		DROP_USER_STMT_LEN_INDEX				(CUD_HEADER_OFFSET + 4	)
#define		GRANT_USER_STMT_LEN_INDEX				(CUD_HEADER_OFFSET + 4	)
#define		ALTER_SESSION_STMT_LEN_INDEX			(CUD_HEADER_OFFSET + 4	)

//			connect modes

#define		USER_MODE								0
#define		SYSDBA_MODE								2

//***	structures are used for explicit conversion

struct ORACLE_VARCHAR 
	{
		unsigned short	len;
		char			seq[1];
	};

typedef struct ORACLE_VARCHAR ORACLE_VARCHAR;

struct ORACLE_VARRAW
	{
		unsigned short	len; 
		unsigned char	seq[1];
	};

typedef struct ORACLE_VARRAW ORACLE_VARRAW;

struct ORACLE_LONG_VARCHAR 
	{
		int				len; 
		char			seq[1];
	};

typedef struct ORACLE_LONG_VARCHAR ORACLE_LONG_VARCHAR;

struct ORACLE_LONG_VARRAW 
	{
		int				len; 
		unsigned char	seq[1];
	};

typedef struct ORACLE_LONG_VARRAW ORACLE_LONG_VARRAW;

struct ORACLE_VARNUM
	{
		unsigned char	len;
		char			seq[1];
	};

typedef struct ORACLE_VARNUM ORACLE_VARNUM;

struct ORACLE_DATE
	{
		unsigned char	seq[7];
	};

typedef struct ORACLE_DATE ORACLE_DATE;

//***

struct ora_cortege
	{
		unsigned int	length;
		void**			values;
		ora_data_type*	values_types;	
	};

typedef struct ora_cortege ora_cortege;

//***			cud command array getters

short*			ora_get_cud_connect				(																		);
short*			ora_get_cud_connect_identified	(																		);
short*			ora_get_cud_commit_work			(																		);
short*			ora_get_cud_commit_work_release	(																		);
short*			ora_get_cud_open_cursor			(short id_cursor, short stmt_len										);
short*			ora_get_cud_close_cursor		(short id_cursor														);
short*			ora_get_cud_fetch_into			(short id_cursor, short param_count, ora_data_type* ora_param_types		);
short*			ora_get_cud_create_table		(short stmt_len															);
short*			ora_get_cud_drop_table			(short stmt_len															);
short*			ora_get_cud_alter_table			(short stmt_len															);
short*			ora_get_cud_update_table		(short stmt_len															);
short*			ora_get_cud_insert_into			(short stmt_len															);
short*			ora_get_cud_delete_from			(short stmt_len															);
short*			ora_get_cud_enable_threads		(																		);
short*			ora_get_cud_create_context		(																		);
short*			ora_get_cud_free_context		(																		);
short*			ora_get_cud_create_user			(short stmt_len															);
short*			ora_get_cud_drop_user			(short stmt_len															);
short*			ora_get_cud_grant_user			(short stmt_len															);
short*			ora_get_cud_alter_session		(short stmt_len															);

//***			types definition

unsigned __int8 type_ora_to_native				(ora_data_type ora_type													);
short			type_ora_to_c					(ora_data_type ora_type													);

//***			exd free

void			ora_free_exd					(exd_struct exd															);

//***			sql query interface

int				ora_enable_threads				(																		);

sql_context		ora_create_context				(																		);
int				ora_free_context				(sql_context context													);

int				ora_exd_connect					(char* stmt, short mode, sql_context* context							);
int				ora_exd_connect_identified		(char* stmtName, char* stmtPass, short mode, sql_context* context		);

int				ora_exd_commit_work				(sql_context* context													);
int				ora_exd_commit_work_release		(sql_context* context													);

ora_cortege*	ora_open_cortege				(unsigned int attribCount, ora_data_type* attribOutTypes				);
void			ora_close_cortege				(ora_cortege* cortege													);

int				ora_exd_open_cursor				(short cursor_id, const char* stmt, sql_context* context				);
exd_struct		ora_exd_fetch_init_cursor		(short cursor_id, ora_cortege*	cortegeInfo								);
int				ora_exd_fetch_cursor			(exd_struct exd, sql_context* context									);
int				ora_exd_close_cursor			(short cursor_id, sql_context* context									);

int				ora_exd_create_table			(const char* stmt, sql_context* context									);
int				ora_exd_drop_table				(const char* stmt, sql_context* context									);
int				ora_exd_alter_table				(const char* stmt, sql_context* context									);
int				ora_exd_update_table			(const char* stmt, sql_context* context									);
int				ora_exd_insert_into				(const char* stmt, sql_context* context									);
int				ora_exd_delete_from				(const char* stmt, sql_context* context									);